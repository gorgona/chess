var currentColor = 'black',
    black = 'black',
    white = 'white',
	dimSquares = [];

function colorize() {
    var rows = $wrap.children;
    for(var i = 0; i < rows.length; i++) {
        var box = rows[i].children;
        if (box.length % 2 === 0) {
        	currentColor = (currentColor === black) ? white: black;
	    }

        for(var j = 0; j < box.length; j++) {
            box[j].style.backgroundColor = currentColor;
            currentColor = (currentColor === black) ? white: black;
        }
    }
}

var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

var $wrap = $('#wrapper');
function createRow () {
	return $('<div>', {
			class: 'row'
		});
}
function createBox (j,i) {
	return $('<div>', {
	        	class: 'box',
	        	id: letters[j] + i
	        });
}
function create () {
	var documentFragment = document.createDocumentFragment();
	for(var i = 8; i > 0; i--) {
		var divRow = createRow();

	    for(var j = 0; j < 8; j++) {
	        var divBox = createBox(j, i)
					divRow.appendChild(divBox);
	    }

	    documentFragment.appendChild(divRow);
	}
	$wrap.appendChild(documentFragment);
}

function Shape () {
	this.el = $('<div>', {
    	class: 'shape'
    });
}


create();
colorize();

//	dimSquares.push(divBox);

function Horse (color, callback) {
	if(color != 'white' && color != 'black') {
		color = 'white';
	}

	Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-horse';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
};

function King (color) {
// code here
	if(color != 'white' && color != 'black') {
		color = 'white';
	}
	Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-king';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
}
function Queen (color) {
	if(color != 'white' && color != 'black') {
		color = 'white';
	}
	Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-queen';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
}
function Rooks (color) {
// code here
	if(color != 'white' && color != 'black') {
		color = 'white';
	}
	Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-rooks';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
}
function Bishops (color) {
// code here
	if(color != 'white' && color != 'black') {
		color = 'white';
	}
	Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-bishops';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
}

function Pawns (color) {
// code here
	if(color != 'white' && color != 'black') {
		color = 'white';
	}

	 Shape.call(this);
	var oldClass = this.el.getAttribute('class');
	var className  = color + '-pawns';
	this.el.setAttribute('class', oldClass + ' ' + className);


	var hungry = false;

	var self = this;
	setTimeout(function () {
		hungry = true;
		event = {
			title: 'hungry',
			target: self
		};
		callback.apply(self, [event]);
	}, 3000);
}
function handlerOnHungry (ev) {
	//console.log(this, arguments);
}

var horseWhite1 = new Horse('white', handlerOnHungry);
var horseWhite2 = new Horse('white', handlerOnHungry);
var queenWhite = new Queen('white', handlerOnHungry);
var kingWhite = new King('white', handlerOnHungry);
var rooksWhite1 = new Rooks('white', handlerOnHungry);
var rooksWhite2 = new Rooks('white', handlerOnHungry);
var bishopsWhite1 = new Bishops('white', handlerOnHungry);
var bishopsWhite2 = new Bishops('white', handlerOnHungry);
var pawnsWhite1 = new Pawns('white', handlerOnHungry);
var pawnsWhite2 = new Pawns('white', handlerOnHungry);
var pawnsWhite3 = new Pawns('white', handlerOnHungry);
var pawnsWhite4 = new Pawns('white', handlerOnHungry);
var pawnsWhite5 = new Pawns('white', handlerOnHungry);
var pawnsWhite6 = new Pawns('white', handlerOnHungry);
var pawnsWhite7 = new Pawns('white', handlerOnHungry);
var pawnsWhite8 = new Pawns('white', handlerOnHungry);

var horseBlack1 = new Horse('black', handlerOnHungry);
var horseBlack2= new Horse('black', handlerOnHungry);
var queenBlack = new Queen('black', handlerOnHungry);
var kingBlack = new King('black', handlerOnHungry);
var rooksBlack1 = new Rooks('black', handlerOnHungry);
var rooksBlack2 = new Rooks('black', handlerOnHungry);
var bishopsBlack1 = new Bishops('black', handlerOnHungry);
var bishopsBlack2 = new Bishops('black', handlerOnHungry);
var pawnsBlack1 = new Pawns('black', handlerOnHungry);
var pawnsBlack2 = new Pawns('black', handlerOnHungry);
var pawnsBlack3 = new Pawns('black', handlerOnHungry);
var pawnsBlack4 = new Pawns('black', handlerOnHungry);
var pawnsBlack5 = new Pawns('black', handlerOnHungry);
var pawnsBlack6 = new Pawns('black', handlerOnHungry);
var pawnsBlack7 = new Pawns('black', handlerOnHungry);
var pawnsBlack8 = new Pawns('black', handlerOnHungry);


var pieses = [rooksWhite1, horseWhite1, bishopsWhite1, queenWhite, kingWhite, bishopsWhite2, horseWhite2, rooksWhite2, pawnsWhite1, pawnsWhite2, pawnsWhite3, pawnsWhite4, pawnsWhite5, pawnsWhite6, pawnsWhite7, pawnsWhite8, rooksBlack1, horseBlack1, bishopsBlack1, kingBlack, queenBlack, bishopsBlack2, horseBlack2, rooksBlack2, pawnsBlack1, pawnsBlack2, pawnsBlack3, pawnsBlack4, pawnsBlack5, pawnsBlack6, pawnsBlack7, pawnsBlack8]
console.log('длина массива фигурок '+pieses.length);


function setUpStandartGame (){
$('#a1').appendChild( rooksWhite1.el );
$('#b1').appendChild( horseWhite1.el );
$('#c1').appendChild( bishopsWhite1.el );
$('#d1').appendChild( queenWhite.el );
$('#e1').appendChild( kingWhite.el );
$('#f1').appendChild( bishopsWhite2.el );
$('#g1').appendChild( horseWhite2.el );
$('#h1').appendChild( rooksWhite2.el );
$('#a2').appendChild( pawnsWhite1.el );
$('#b2').appendChild( pawnsWhite2.el );
$('#c2').appendChild( pawnsWhite3.el );
$('#d2').appendChild( pawnsWhite4.el );
$('#e2').appendChild( pawnsWhite5.el );
$('#f2').appendChild( pawnsWhite6.el );
$('#g2').appendChild( pawnsWhite7.el );
$('#h2').appendChild( pawnsWhite8.el );

$('#a8').appendChild( rooksBlack1.el );
$('#b8').appendChild( horseBlack1.el );
$('#c8').appendChild( bishopsBlack1.el );
$('#d8').appendChild( queenBlack.el );
$('#e8').appendChild( kingBlack.el );
$('#f8').appendChild( bishopsBlack2.el );
$('#g8').appendChild( horseBlack2.el );
$('#h8').appendChild( rooksBlack2.el );
$('#a7').appendChild( pawnsBlack1.el );
$('#b7').appendChild( pawnsBlack2.el );
$('#c7').appendChild( pawnsBlack3.el );
$('#d7').appendChild( pawnsBlack4.el );
$('#e7').appendChild( pawnsBlack5.el );
$('#f7').appendChild( pawnsBlack6.el );
$('#g7').appendChild( pawnsBlack7.el );
$('#h7').appendChild( pawnsBlack8.el );
	}


setUpStandartGame();
function in_array(value, array)
{
	for(var i = 0; i < array.length; i++)
	{
		if(array[i] == value) return true;
	}
	return false;
}

function setUpRandomGame() {
	var letter;
	var index;
	var randomPosition=[];
	var i=0
	while ( i < 32 ) {
		//console.log('фигурка '+pieses[i]);
		letter = letters[Math.round(Math.random() * 7, 0)];
		index1 = Math.round(Math.random() * 7, 0)+1;

		if(in_array(letter+index1,randomPosition) )
		{
			// ничего не делаю
			//console.log('уже есть ');
		}
		else {
			randomPosition[i] = letter + index1;

			$('#' + letter + index1).appendChild(pieses[i].el);
			//console.log(letter + index1+'i '+i);
			i++;
		}
	}
//console.log('длина массива позиций '+randomPosition.length);
}
	setUpRandomGame();

function moveRandom(pies){
	while (free())
	{
		letter = letters[Math.round(Math.random() * 7, 0)];
		index1 = Math.round(Math.random() * 7, 0)+1;
		

}

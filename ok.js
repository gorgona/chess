(function iife (global) {	
	function ok (argument, attributes) {

		if(argument.indexOf('<') === 0 &&
			argument.indexOf('>') === argument.length-1) {
			
			argument = argument.
						replace('<', '').replace('>', '');

			var newElement = document.createElement(argument);

			if (attributes) {
				for (key in attributes) {
					var value = attributes[key];
					newElement.setAttribute(key, value);
				}	
			}

			return newElement;
		} else {
			return document.querySelector(argument);
		}
	}

	global.$ = global.ok = ok;
})(window);
